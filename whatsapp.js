
$(document).ready(function(){
    // INPUT CHAT MESSAGES, SENDING AND RECEIVING A MESSAGE

    $('#input-message').keypress(function(e){
          if (e.which == 13) {
            // This date will indicate the time at which the message will be sent and received
            var now = new Date();
            // Append to the message-container the same structure of the static HTML
            // message sent and received
            $(".message-container.active").append('<div class="clear"></div>' +
              '<div class="message-sent">' +
                '<div class = "text-sent">'+ $('#input-message').val() +
                  '<div class="message-time">'+ now.getHours() + ":" + now.getMinutes() + '</div>'+
                  '<i class="fas fa-caret-down">'+'</i>' +
                '</div>' +
                '<div class="message-options">' + '</div>' +
              '</div>' +
            '<div class="clear"></div>');
            // When the message is sent, the input is empty again
            $('#input-message').val("");
            // Automatic scroll to the last message in the message-container
            $(".message-container").scrollTop($(".message-container")[0].scrollHeight)
            // AJAX CALLS THAT RETURNS RANDOM PHRASES
            $.ajax({
              url : "https://www.boolean.careers/api/random/sentence",
              success : function(data,stato) {
                // salvo in una var globale il valore proveniente dal server api
                randomPhrase = data.response;
                console.log(randomPhrase);
              },
            });
            // I create a setTimeOut function that send a message after 2000 millisecond
            // an input message chat user send a text
            setTimeout(function(){
              $(".message-container.active").append('<div class="message-received">' +
                 '<div id ="message-received" class="message-received" style = "padding:0">' +
                    '<div class = text-received>' + randomPhrase +
                      '<div class="message-time-sent">'+ now.getHours() + ":" + now.getMinutes() + '</div>'
                  + '</div class>' +
                 '</div>' +
              '</div>'
              + '<div class = "clear"></div>');
            }, 2000);
          }
    });


    // FILTER TO FIND FRIENDS
    $("#input-friends").keyup(function(){
      var searchFriend =   $("#input-friends").val().toLowerCase();
      var allFriends = $(".friend-container");

      allFriends.each(function(){
        var thisFriendName = $(this).children(".friend-name").children(".title");
        if (!thisFriendName.text().toLowerCase().includes(searchFriend)) {
  				$(this).hide();
  			}
  			else {
  				$(this).show();
  			}
      });
    });
    // CHANGE DISLAY CHAT FOR EACH FRIEND
    $(".friend-container").each(function(){
      $(this).click(function(){

        //Change the name in the chat header bar when I click a friend
		    var friendName = $(this).children('.friend-name').children('.title').text();
		    $('.name-contact').text(friendName);
        // Hide each chat
        $("#chat1").hide();
        $("#chat2").hide();
        $("#chat3").hide();
        $("#chat4").hide();
        $("#chat1").hide();
        $("#chat5").hide();
        $("#chat6").hide();
        $("#chat7").hide();
        $("#chat8").hide();
        $("#chat1").removeClass("active")
        $("#chat2").removeClass("active")
        $("#chat3").removeClass("active")
        $("#chat4").removeClass("active")
        $("#chat5").removeClass("active")
        $("#chat6").removeClass("active")
        $("#chat7").removeClass("active")
        $("#chat8").removeClass("active")

        if($(this).hasClass("friend1")) {
            $("#chat1").show();
            $("#chat1").addClass("active");
        }
        if($(this).hasClass("friend2")) {
            $("#chat2").show();
            $("#chat2").addClass("active");

        }
        if($(this).hasClass("friend3")) {
            $("#chat3").show();
            $("#chat3").addClass("active");

        }
        if($(this).hasClass("friend4")) {
            $("#chat4").show();
            $("#chat4").addClass("active");

        }
        if($(this).hasClass("friend5")) {
            $("#chat5").show();
            $("#chat5").addClass("active");

        }
        if($(this).hasClass("friend6")) {
            $("#chat6").show();
            $("#chat6").addClass("active");


        }
        if($(this).hasClass("friend7")) {
            $("#chat7").show();
            $("#chat7").addClass("active");
        }
        if($(this).hasClass("friend8")) {
            $("#chat8").show();
            $("#chat8").addClass("active");
        }
      });
    });
    $(document).on("mouseenter",".message-sent",function(){
      var currentIcon= $(this).find(".fas.fa-caret-down ");
        currentIcon.show();
    });
    $(document).on("mouseleave",".message-sent",function(){
      var currentIcon= $(this).find(".fas.fa-caret-down ");
        currentIcon.hide();
    });

});
